
import scipy.io as sio
from config import *
import numpy as np
import random
import cv2
#a
def apply_metrics(img_original, img_rec):
    from sklearn.metrics import mean_squared_error
    from sewar.full_ref import psnr,mse,ssim

    score = {'mse':None, 'psnr': None, 'ssim': None}

    score['mse'] = mse(img_original, img_rec)
    score['psnr'] = psnr(img_original, img_rec)
    score['ssim'] = ssim(img_original, img_rec)[0] #:returns:  tuple -- ssim value, cs value.

    return score

def get_reconstructed_image(weakly_illu_image, L):
    if weakly_illu_image is not None:
        I = np.copy(weakly_illu_image).astype(np.float32)
        I /= np.max(I)

        R = I/L
        R /= np.max(R)
        R = (R*255).astype(np.uint8)

        return R

def apply_illumination(source_image, value):
    # value = random.uniform(0.1, 0.7)
    dark_image = np.copy(source_image).astype(np.float32)

    dark_image /= np.max(dark_image) #Normalizando 0-1
    dark_image *= value
    dark_image = np.clip(dark_image, a_min=0, a_max=1)

    return (dark_image*255).astype(np.uint8)

def augment_color(weakly_image, illu):
    random.seed(2)
    # color_aug = random.uniform(0.05, 0.3)
    color_aug = -(random.random() - 0.5)

    #Recovery orgin image:
    new_image = get_reconstructed_image(weakly_image, illu).astype(np.float32)

    new_image /= np.max(new_image)  # Normalizando 0-1

    new_illu = illu
    new_illu += illu*color_aug

    return apply_illumination(new_image, new_illu), new_illu


if __name__ == '__main__':
    filenames = sio.loadmat(DATASET_DIR + "/folds.mat")['Xfiles']
    illu_gd = sio.loadmat(DATASET_DIR + "/ground_truth.mat")['illu'][0]

    or_image = cv2.imread(DATASET_DIR + '/original/' + filenames[1])
    wk_image = cv2.imread(DATASET_DIR + '/synthetic/' + filenames[1])
    illu = illu_gd[1]


    # rc_image = get_reconstructed_image(wk_image, illu)
    # m1 = apply_metrics(or_image, rc_image) #medidas

    new_image, new_illu = augment_color(wk_image, illu)
    img2 = apply_illumination(or_image,illu)
    rc_image2 = get_reconstructed_image(new_image, new_illu)

    m2 = apply_metrics(or_image, rc_image2) #medidas

    cv2.imshow('images', np.hstack([cv2.resize(or_image, (200, 200)),
                                    cv2.resize(wk_image, (200, 200)),
                                    cv2.resize(new_image, (200, 200)),
                                    cv2.resize(img2, (200, 200))]))
    cv2.waitKey(0)

    print()