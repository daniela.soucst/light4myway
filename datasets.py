import cv2
import cPickle as pickle
import scipy.io
import numpy as np
import os
import sys
import random
from utils import slice_list
from image_generator import dataset_split
SHOW_IMAGES = False
FOLDS = 3

DATA_FRAGMENT = -1
BOARD_FILL_COLOR = 1e-5


def get_image_pack_fn(key):
  ds = key[0]

  if ds == 'b':
    fold = int(key[1])
    return BSDSDataSet().get_image_pack_fn(fold)
  # elif ds == 'c':
  #   camera = int(key[1])
  #   fold = int(key[2])
  #   return ChengDataSet(camera).get_image_pack_fn(fold)
  elif ds == 'm':
    assert False


class ImageRecord:
  def __init__(self, dataset, fn, illum, img, extras=None):
    self.dataset = dataset
    self.fn = fn
    self.illum = illum # L (escalar)
    # BRG images
    self.img = img
    self.extras = extras

  def __repr__(self):
    return '[%s, %s, %.4f]' % (self.dataset, self.fn, self.illum)


class DataSet:

  def get_subset_name(self):
    return ''

  def get_directory(self):
    return 'data/' + self.get_name() + '/'

  def get_img_directory(self):
    return 'data/' + self.get_name() + '/'

  def get_meta_data_fn(self):
    return self.get_directory() + self.get_subset_name() + 'meta.pkl'

  def dump_meta_data(self, meta_data):
    print 'Dumping data =>', self.get_meta_data_fn()
    print '  Total records:', sum(map(len, meta_data))
    print '  Slices:', map(len, meta_data)
    with open(self.get_meta_data_fn(), 'wb') as f:
      pickle.dump(meta_data, f, protocol=-1)
    print 'Dumped.'

  def load_meta_data(self):
    with open(self.get_meta_data_fn()) as f:
      return pickle.load(f)

  def get_image_pack_fn(self, fold):
    return self.get_directory() + self.get_subset_name(
    ) + 'image_pack.%d.pkl' % fold

  def dump_image_pack(self, image_pack, fold):
    with open(self.get_image_pack_fn(fold), 'wb') as f:
      pickle.dump(image_pack, f, protocol=-1)

  def load_image_pack(self, fold):
    with open(self.get_meta_data_fn()) as f:
      return pickle.load(f)

  def regenerate_image_pack(self, meta_data, fold):
    image_pack = []
    for i, r in enumerate(meta_data):
      print 'Processing %d/%d\r' % (i + 1, len(meta_data)),
      sys.stdout.flush()
      r.img = self.load_image(r)
      image_pack.append(r)

    self.dump_image_pack(image_pack, fold)

  def regenerate_image_packs(self):
    meta_data = self.load_meta_data()
    print 'Dumping image packs...'
    print '%s folds found' % len(meta_data)
    for f, m in enumerate(meta_data):
      self.regenerate_image_pack(m, f)

  def get_folds(self):
    return FOLDS


class BSDSDataSet(DataSet):

  def get_name(self):
    return 'BSDS500'

  def regenerate_meta_data(self):
    meta_data = []
    print "Loading and shuffle fn_and_illum[]"
    ground_truth = scipy.io.loadmat(self.get_directory() + 'ground_truth.mat')['illu'][0]
    filenames = sorted(os.listdir(self.get_directory()+'synthetic'))
    folds = scipy.io.loadmat(self.get_directory() + 'folds.mat')
    filenames2 = map(lambda x: str(x).strip(), folds['Xfiles'])

    for i in range(len(filenames)):
      assert filenames[i][:-4] == filenames2[i][:-4]
    for i in range(len(filenames)):
      fn = filenames[i]
      meta_data.append(
          ImageRecord(
              dataset=self.get_name(),
              fn=fn,
              illum=ground_truth[i],
              img=None))

    if DATA_FRAGMENT != -1:
      meta_data = meta_data[:DATA_FRAGMENT]
      print 'Warning: using only first %d images...' % len(meta_data)

    meta_data_folds = [[], [], []]
    for i in range(FOLDS):
      fold = list(folds['te_split'][0][i][0])
      print len(fold)
      for j in fold:
        meta_data_folds[i].append(meta_data[j - 1])
    for i in range(3):
      print 'Fold', i
      print map(lambda m: m.fn, meta_data_folds[i])
    print sum(map(len, meta_data_folds))
    assert sum(map(len, meta_data_folds)) == len(filenames)
    for i in range(3):
      assert set(meta_data_folds[i]) & set(meta_data_folds[(i + 1) % 3]) == set(
      )
    self.dump_meta_data(meta_data_folds)

  def load_image(self, r):
    file_path = self.get_img_directory() + '/synthetic/' + r.fn
    raw = np.array(cv2.imread(file_path, -1), dtype='float32')
    raw = raw.astype(np.uint16)
    return raw

  # def load_image_without_mcc(self, r):
  #   raw = self.load_image(r.fn)
  #   img = (np.clip(raw / raw.max(), 0, 1) * 65535.0).astype(np.uint16)
  #   polygon = r.mcc_coord * np.array([img.shape[1], img.shape[0]])
  #   polygon = polygon.astype(np.int32)
  #   cv2.fillPoly(img, [polygon], (BOARD_FILL_COLOR,) * 3)
  #   return img




if __name__ == '__main__':
  # ds = GehlerDataSet()
  ds = BSDSDataSet()
  ds.regenerate_meta_data()
  ds.regenerate_image_packs()
