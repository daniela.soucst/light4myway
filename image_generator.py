# -*- coding: utf-8 -*-
import glob
import os
import cv2
import numpy as np
import random
# import statistics
from config import *
from os import listdir
from os.path import isfile, join
import scipy.io as sio
#Para geracao de folds.mat
from sklearn.model_selection import KFold

random.seed(0)

def turn_off_ligths(source_image):
    value = random.uniform(0.1, 0.7)
    dark_image = np.copy(source_image).astype(np.float32)

    dark_image /= np.max(dark_image) #Normalizando 0-1
    dark_image *= value
    dark_image = np.clip(dark_image, a_min=0, a_max=1)

    return (dark_image*255).astype(np.uint8), value

def generate_synthetic_image(img_name):
    original = cv2.imread(img_name)

    if original is not None:
        transformed_img, l = turn_off_ligths(original)
        return transformed_img

def generate_synthetic_images():

    # images = os.listdir(DATASET_DIR+'/*jpg')
    # images = glob.glob(DATASET_DIR+'/*.jpg')
    images = sorted([f for f in listdir(DATASET_DIR+'/original') if isfile(join(DATASET_DIR+'/original', f))])

    systhetic_folder = os.path.join(DATASET_DIR, 'synthetic')
    ground_truth = []
    xfiles = []
    try:
        os.mkdir(systhetic_folder)

    except:
        pass

    for image_name in images:

        original = cv2.imread(os.path.join(DATASET_DIR, 'original', image_name))
        transformed_img, l = turn_off_ligths(original)
        cv2.imwrite(os.path.join(systhetic_folder, image_name), transformed_img)
        ground_truth.append(l)
        xfiles.append(image_name)

    sio.savemat(DATASET_DIR+"/ground_truth.mat", {'illu': np.asarray(ground_truth)})
    return images


def dataset_split(filenames):
    X = np.array(filenames)
    # y = np.array([1, 2, 3, 4])
    kf = KFold(n_splits=3)
    kf.get_n_splits(X)

    print(kf)
    tr_split = []
    te_split = []

    for train_index, test_index in kf.split(X):
        tr_split.append(np.array(train_index))
        te_split.append(np.array(test_index))


    sio.savemat(DATASET_DIR + "/folds.mat",  {'Xfiles': np.array(filenames),
                                              'te_split': np.array([te_split]), 'tr_split': np.array([tr_split])})

def augment_color():
    filenames = sio.loadmat(DATASET_DIR + "/folds.mat")['Xfiles']
    illu_gd = sio.loadmat(DATASET_DIR + "/ground_truth.mat")['illu']


if __name__ == '__main__':
    # filenames = generate_synthetic_images()
    # dataset_split(filenames)

    augment_color()